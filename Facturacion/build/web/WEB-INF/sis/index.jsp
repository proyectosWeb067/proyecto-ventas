<%-- 
    Document   : principal
    Created on : 24/08/2020, 10:22:03 PM
    Author     : Anonyman
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--Css-estilos-->
        <link rel="stylesheet" href="css/estilos.css" type="text/css">
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">

        <title>Facturacion</title>
    </head>
    <body>

        <div class="container" >
            <div class="card" id="fondo">

                <div class="card-body">
                    <form id="login" class="bordeFrmLogin" method="POST" action="Login">
                        <input type="hidden" name="accion" value="vbs"/>
                        <div class="form-group">
                            <h3 id="login">Login</h3>
                            <img  class="tamanoImagen" src="img/imgT.png"/><br/>
                            <h3 id="login">Bienvenido</h3>
                        </div>
                        <div class="form-group">
                            <label>Usuario</label><br/>
                            <label class="text-warning">${msu}</label>
                            <input id="usuario" name="usuario" required="true" type="text" class="form-control" placeholder="Usuario."/>

                        </div>
                        <div class="form-group">
                            <label>Contraseña</label><br/>
                            <label class="text-warning">${msp}</label>
                            <input id="password"name="password" required="true" type="password" class="form-control" placeholder="Contraseña."/>

                        </div>

                        <div class="form-actions">
                            <button onclick="up()"type="submit" class="btn btn-success">Ingresar</button>                        
                        </div>

                    </form>
                    <h4 class="text-warning text-center">${msj}</h4>
                    <h4 class="text-warning text-center" id="msgout"></h4>
                    <h4 class="text-warning text-center" id="bdO">${bdOut}</h4>
                </div>
            </div>
        </div>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/validation.js"></script>
    </body>
</html>
