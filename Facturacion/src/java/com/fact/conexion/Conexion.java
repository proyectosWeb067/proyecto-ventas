/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fact.conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Anonyman, aplicando patron singleton
 */
public class Conexion {

    private final String USUARIO = "root";
    private final String PASS = "";
    private final String URL = "jdbc:mysql://localhost:3306/dbfact";
    private final String DRIVER = "com.mysql.jdbc.Driver";
    
    private Connection con;
    public static Conexion conexion; //variable tipo conexion.
   
    private Conexion(){
        try {
             Class.forName(DRIVER);
            con = DriverManager.getConnection(URL, USUARIO, PASS);
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public synchronized static Conexion estadoConexion(){
        if(conexion == null){
            conexion = new Conexion();
        }
        return conexion;
    }
    
    public Connection conectar(){
       return con;
    }
    
    public void cerrar(){
        
        conexion = null;
    } 
   
}
