/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fact.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * WebFilter(filterName = "LoginFilter", urlPatterns = {"/sis/*"}
 *
 * @author ya
 */
@WebFilter(filterName = "LoginFilter", urlPatterns = {"/sis/*"})
public class LoginFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        String nombreFiltro = filterConfig.getFilterName();
        System.out.println("-------> " + nombreFiltro);

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        //obteniendo la sesion 
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpSession session = httpRequest.getSession();

        if (session.getAttribute("empleado") != null) {
            chain.doFilter(request, response);
        } else {
                String error = "¡Ingrese al sistema, con sus credenciales!";
                session.setAttribute("err", error);
                RequestDispatcher r = httpRequest.getRequestDispatcher("/WEB-INF/sis/rq_permiso/rp_out.jsp");
                r.forward(request, response);
        }

    }

    @Override
    public void destroy() {
    }

}
