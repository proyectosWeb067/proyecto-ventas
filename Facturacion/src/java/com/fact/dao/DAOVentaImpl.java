/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fact.dao;

import com.fact.conexion.Conexion;
import com.fact.model.Venta;
import java.util.List;

/**
 *
 * @author ya
 */
public class DAOVentaImpl implements DAOVenta{

    private final Conexion CONEXION = Conexion.estadoConexion();
    
    //SQL consultas
    private final String OBTENER_TODOS ="SELECT * FROM VENTA";
    private final String OBTENER = "SELECT * FROM VENTA WHERE numeroFactura = ?";
    private final String INSERTAR ="INSERTAR INTO VENTA (numeroFactura,fechaVenta,precio,estado) VALUES(?,?,?,?)";
    private final String ELIMINAR ="DELETE FROM VENTA WHERE numeroFactura =?";
     
    @Override
    public List obtenerTodo() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Venta obtener(int key) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void insertar(Venta v) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actualizar(Venta v) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void eliminar(Venta v) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
