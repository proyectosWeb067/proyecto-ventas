/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fact.dao;

import com.fact.model.Empleado;

/**
 *
 * @author ya
 */
public interface DAOEmpleado extends CRUD<Empleado>{
    
    Empleado login(String u,String p);
}
