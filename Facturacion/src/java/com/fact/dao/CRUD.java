/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fact.dao;

import java.util.List;

/**
 *
 * @author ya
 * @param <T> utilizando generics, para codigo comun.
 */
public interface CRUD<T> {

    List<T> obtenerTodo();

    T obtener(int key);

    void insertar(T t);

    void actualizar(T t);

    void eliminar(T t);
}
