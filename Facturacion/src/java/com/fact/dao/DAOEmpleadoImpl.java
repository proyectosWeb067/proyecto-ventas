/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fact.dao;

import com.fact.conexion.Conexion;
import com.fact.model.Empleado;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ya
 */
public class DAOEmpleadoImpl implements DAOEmpleado {

    private final Conexion CONEXION = Conexion.estadoConexion();

    //SQL consultas
    private final String OBTENER_TODOS = "SELECT * FROM EMPLEADO";
    private final String OBTENER = "SELECT * FROM EMPLEADO WHERE usuario = ?";
    private final String INSERTAR = "INSERT INTO EMPLEADO (nombre,apellido,cedula,telefono,direccion,usuario,password) VALUES (?,?,?,?,?,?,?)";
    private final String ACTUALIZAR = "UPDATE EMPLEADO SET (nombre=?,apellido=?,cedula=?,telefono=?,direccion=?,usuario=?,password=?) WHERE  cedula = ?";
    private final String ELIMINAR = "DELETE FROM EMPLEADO WHERE cedula =?";
    private final String LOG = "SELECT * FROM EMPLEADO where usuario=? AND password=? ";

    @Override

    public List obtenerTodo() {
        List<Empleado> lista = new ArrayList();
        try {
            PreparedStatement ps = CONEXION.conectar().prepareStatement(OBTENER_TODOS);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Empleado e = new Empleado();
                e.setId_empleado(rs.getInt(1));
                e.setNombre(rs.getString(2));
                e.setApellido(rs.getString(3));
                e.setCedula(rs.getInt(4));
                e.setTelefono(rs.getInt(5));
                e.setDireccion(rs.getString(6));
                e.setUsuario(rs.getString(7));
                lista.add(e);
            }
            ps.close();
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOEmpleadoImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            CONEXION.cerrar();
        }
        return lista;
    }

    @Override
    public Empleado obtener(int key) {
        Empleado e = null;
        try {
            PreparedStatement ps = CONEXION.conectar().prepareStatement(OBTENER);
            ps.setInt(1, key); // pasando parametro a consulta
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                e = new Empleado();
                e.setNombre(rs.getString(2));
                e.setApellido(rs.getString(3));
                e.setCedula(rs.getInt(4));
                e.setTelefono(rs.getInt(5));
                e.setDireccion(rs.getString(6));
                e.setUsuario(rs.getString(7));
            }
            ps.close();
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOEmpleadoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return e;
    }

    @Override //quiza pueda retornar un mensaje, de validacion
    public void insertar(Empleado e) {
        try {
            PreparedStatement ps = CONEXION.conectar().prepareStatement(INSERTAR);
            ps.setString(1, e.getNombre());
            ps.setString(2, e.getApellido());
            ps.setInt(3, e.getCedula());
            ps.setInt(4, e.getTelefono());
            ps.setString(5, e.getDireccion());
            ps.setString(7, e.getUsuario());
            ps.setString(8, e.getPassword());
            ps.executeUpdate();
            ps.close();

        } catch (SQLException ex) {
            Logger.getLogger(DAOEmpleadoImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            CONEXION.cerrar();
        }
    }

    @Override
    public void eliminar(Empleado e) {
        try {
            PreparedStatement ps = CONEXION.conectar().prepareStatement(ELIMINAR);
            ps.setInt(1, e.getCedula());
            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOEmpleadoImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            CONEXION.cerrar();
        }
    }

    @Override
    public void actualizar(Empleado e) {
        try {
            PreparedStatement ps = CONEXION.conectar().prepareStatement(ACTUALIZAR);
            ps.setString(1, e.getNombre());
            ps.setString(2, e.getApellido());
            ps.setInt(3, e.getCedula());
            ps.setInt(4, e.getTelefono());
            ps.setString(5, e.getDireccion());
            ps.setString(6, e.getUsuario());
            ps.setString(7, e.getPassword());
            ps.setInt(8, e.getCedula());
            ps.executeUpdate();
            ps.close();

        } catch (SQLException ex) {
            Logger.getLogger(DAOEmpleadoImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            CONEXION.cerrar();
        }

    }

    @Override
    public Empleado login(String u, String p) {
        Empleado empleado = null;
        try {
            PreparedStatement ps = CONEXION.conectar().prepareStatement(LOG);
            ps.setString(1, u);
            ps.setString(2, p);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                empleado = new Empleado();
                empleado.setNombre(rs.getString(2));
                empleado.setApellido(rs.getString(3));
                empleado.setCedula(rs.getInt(4));
                empleado.setTelefono(rs.getInt(5));
                empleado.setDireccion(rs.getString(6));
                empleado.setUsuario(rs.getString(7));
                empleado.setPassword(rs.getString(8));
            }
            ps.close();
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOEmpleadoImpl.class.getName()).log(Level.SEVERE, null, ex);
           
        }finally{
            CONEXION.cerrar();
        }
        return empleado;
    }

}
