
package com.fact.dao;

import com.fact.model.Venta;

/**
 *
 * @author ya
 * utilizando generics, de esta manera me aseguro que la interfaces
 * tenga sus propios metodos y herede los que son comunes 
 */
public interface DAOVenta extends CRUD<Venta>{
    
}
