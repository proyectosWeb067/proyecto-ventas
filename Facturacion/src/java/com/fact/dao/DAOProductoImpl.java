package com.fact.dao;

import com.fact.conexion.Conexion;
import com.fact.model.Producto;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ya
 */
public class DAOProductoImpl implements DAOProducto {

    private final Conexion CONEXION = Conexion.estadoConexion();

    //SQL consultas
    private final String OBTENER_TODOS = "SELECT * FROM PRODUCTO";
    private final String OBTENER = "SELECT * FROM PRODUCTO WHERE id_producto = ?";
    private final String INSERTAR = "INSERT INTO PRODUCTO (nombre,codigo,precio,stock,estado) VALUES (?,?,?,?,?)";
    private final String ELIMINAR = "DELETE FROM PRODUCTO WHERE id_producto = ?";
    private final String ACTUALIZAR = "UPDATE PRODUCTO SET nombre =?,codigo=?,precio=?,stock=?,estado=? WHERE id_producto = ?";

    @Override
    public List obtenerTodo() {
        List<Producto> lista = new ArrayList();
        try {
            PreparedStatement ps = CONEXION.conectar().prepareStatement(OBTENER_TODOS);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Producto p = new Producto();
                p.setId_producto(rs.getInt(1));
                p.setNombre(rs.getString(2));
                p.setCodigo(rs.getString(3));
                p.setPrecio(rs.getDouble(4));
                p.setStock(rs.getInt(5));
                p.setEstado(rs.getString(6));
                lista.add(p);
            }
            ps.close();
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOProductoImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            CONEXION.cerrar();
        }

        return lista;
    }

    @Override
    public Producto obtener(int key) {
        Producto p = null;
        try {
            PreparedStatement ps = CONEXION.conectar().prepareStatement(OBTENER);
            ps.setInt(1, key);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                p = new Producto();
                p.setId_producto(rs.getInt(1));
                p.setNombre(rs.getString(2));
                p.setCodigo(rs.getString(3));
                p.setPrecio(rs.getDouble(4));
                p.setStock(rs.getInt(5));
                p.setEstado(rs.getString(6));
            }
            ps.close();
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOProductoImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            CONEXION.cerrar();
        }
        return p;
    }

    @Override
    public void insertar(Producto p) {
        try {
            PreparedStatement ps = CONEXION.conectar().prepareStatement(INSERTAR);
            ps.setString(1, p.getNombre());
            ps.setString(2, p.getCodigo());
            ps.setDouble(3, p.getPrecio());
            ps.setInt(4, p.getStock());
            ps.setString(5, p.getEstado());
            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOProductoImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            CONEXION.cerrar();
        }

    }

    @Override
    public void actualizar(Producto p) {
        try {
            PreparedStatement ps = CONEXION.conectar().prepareStatement(ACTUALIZAR);
            ps.setString(1, p.getNombre());
            ps.setString(2, p.getCodigo());
            ps.setDouble(3, p.getPrecio());
            ps.setInt(4, p.getStock());
            ps.setString(5, p.getEstado());
            ps.setInt(6, p.getId_producto());
            ps.executeUpdate(); // puedo hacer una validacion devulve 1
            ps.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOProductoImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            CONEXION.cerrar();
        }

    }

    @Override
    public void eliminar(Producto p) {
        try {
            PreparedStatement ps = CONEXION.conectar().prepareStatement(ELIMINAR);
            ps.setInt(1, p.getId_producto());
            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOProductoImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            CONEXION.cerrar();
        }

    }

}
