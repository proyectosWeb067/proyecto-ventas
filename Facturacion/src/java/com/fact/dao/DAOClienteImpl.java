/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fact.dao;

import com.fact.conexion.Conexion;
import com.fact.model.Cliente;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ya
 */
public class DAOClienteImpl implements DAOCliente {

    private final Conexion CONEXION = Conexion.estadoConexion();

    //SQL consultas
    private final String OBTENER_TODOS = "SELECT * FROM CLIENTE";
    private final String OBTENER = "SELECT * FROM CLIENTE WHERE cedula = ?";
    private final String INSERTAR = "INSERT INTO (nombre,apellido,cedula,direccion,ciudad) VALUES (?,?,?,?,?)";
    private final String ACTUALIZAR = "UPDATE CLIENTE SET (nombre=?,apellido=?,cedula=?,direccion=?,ciudad=?) where cedula = ?";
    private final String ELIMINAR = "DELETE FROM CLIENTE WHERE cedula = ?";

    @Override
    public List obtenerTodo() {
        List<Cliente> lista = new ArrayList();
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = CONEXION.conectar().prepareStatement(OBTENER_TODOS);
            rs = ps.executeQuery();
            while (rs.next()) {
                Cliente c = new Cliente();
                c.setId_cliente(rs.getInt(1));
                c.setNombre(rs.getString(2));
                c.setApellido(rs.getString(3));
                c.setCedula(rs.getInt(4));
                c.setDireccion(rs.getString(5));
                c.setCiudad(rs.getString(6));
                lista.add(c);
            }
            rs.close();
            ps.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOClienteImpl.class.getName()).log(Level.SEVERE, null, ex);

        } finally {
            CONEXION.cerrar();
        }
        return lista;
    }

    @Override
    public Cliente obtener(int key) {
        Cliente c = null;
        try {
            PreparedStatement ps = CONEXION.conectar().prepareStatement(OBTENER);
            ps.setInt(1, key);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                c = new Cliente();
                c.setNombre(rs.getString(2));
                c.setApellido(rs.getString(3));
                c.setCedula(rs.getInt(4));
                c.setDireccion(rs.getString(5));
                c.setCiudad(rs.getString(6));
            }
            ps.close();
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOClienteImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            CONEXION.cerrar();
        }
        return c;
    }

    @Override
    public void insertar(Cliente c) {
        try {
            PreparedStatement ps = CONEXION.conectar().prepareStatement(INSERTAR);
            ps.setString(1, c.getNombre());
            ps.setString(2, c.getApellido());
            ps.setInt(3, c.getCedula());
            ps.setString(4, c.getDireccion());
            ps.setString(5, c.getCiudad());

            ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(DAOClienteImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            CONEXION.cerrar();
        }
    }

    @Override
    public void actualizar(Cliente c) {
        try {
            PreparedStatement ps = CONEXION.conectar().prepareStatement(ACTUALIZAR);
            ps.setString(1, c.getNombre());
            ps.setString(2, c.getApellido());
            ps.setInt(3, c.getCedula());
            ps.setString(4, c.getDireccion());
            ps.setString(5, c.getCiudad());
            ps.setInt(6, c.getCedula());
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAOClienteImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            CONEXION.cerrar();
        }
    }

    @Override
    public void eliminar(Cliente c) {
        try {
            PreparedStatement ps = CONEXION.conectar().prepareStatement(ELIMINAR);
            ps.setInt(1, c.getCedula());
        } catch (SQLException ex) {
            Logger.getLogger(DAOClienteImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            CONEXION.cerrar();
        }

    }

   

}
