/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fact.controller;

import com.fact.dao.DAOProducto;
import com.fact.dao.DAOProductoImpl;
import com.fact.model.Producto;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ya
 */
@WebServlet(name = "ServletPage", urlPatterns = {"/Page"})
public class ServletPage extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        //acceso a recurso atraves de servlet

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

        try {
            String acc = request.getParameter("req");
            if (acc != null) {
                switch (acc) {
                    case "product":
                        response.sendRedirect("Producto?search=productos");
                        break;
                    case "home":
                        request.getRequestDispatcher("/WEB-INF/sis/Home.jsp").forward(request, response);
                        break;
                    case "client":
                        request.getRequestDispatcher("/WEB-INF/sis/Clientes.jsp").forward(request, response);
                        break;
                    case "users":
                        request.getRequestDispatcher("/WEB-INF/sis/Usuarios.jsp").forward(request, response);
                        break;
                    case "sale":
                        request.getRequestDispatcher("/WEB-INF/sis/Venta.jsp").forward(request, response);
                        break;
                    default:
                        request.getRequestDispatcher("/WEB-INF/sis/Home.jsp").forward(request, response);
                        break;
                }
            } else {
                request.getRequestDispatcher("/WEB-INF/sis/Home.jsp").forward(request, response);
                System.out.println("en seccion page");
            }
        } catch (Exception e) {
            System.out.println("ExcptionError - " +e.getMessage() );
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
