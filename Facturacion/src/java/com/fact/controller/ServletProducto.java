/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fact.controller;

import com.fact.dao.DAOProducto;
import com.fact.dao.DAOProductoImpl;
import com.fact.model.Producto;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ya
 */
@WebServlet(name = "ServletProducto", urlPatterns = {"/Producto"})
public class ServletProducto extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String accget = request.getParameter("search");
            switch (accget) {
                case "productos":
                    List<Producto> lista = allProduct();
                    request.getSession().setAttribute("lstp", lista);
                    request.getRequestDispatcher("/WEB-INF/sis/Productos.jsp").forward(request, response);
                    String url = request.getServletPath();
                    System.out.println("URL--->" + url);
                    break;
                case "prodjson":
                    listarProductos(request, response);
                    break;
                default:
                    request.getRequestDispatcher("/WEB-INF/sis/Home.jsp").forward(request, response);
                    break;
            }
        } catch (Exception e) {
            request.getRequestDispatcher("/WEB-INF/sis/Home.jsp").forward(request, response);

            System.out.println("Mensa--> " + e.getMessage());
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

        DAOProducto pDao = new DAOProductoImpl();

        String actionRequest = request.getParameter("actionp");

        System.out.println("acccion--> " + actionRequest);
        switch (actionRequest) {
            case "save":
                Producto pS = new Producto();
                String msjValidacionSave = validacionSave(pS, request);
                guardarProducto(pS, pDao, msjValidacionSave, response);
                break;
            case "edit":
                int id_produc = Integer.parseInt(request.getParameter("inep"));
                buscarProducto(response, id_produc, pDao);
                break;
            case "update":
                Producto pU = new Producto();
                String msjValidacionUpdate = validacionUpdate(pU, request);
                actualizarProducto(response, msjValidacionUpdate, pU, pDao);
                break;
            case "del":
                int codigoEliminar = Integer.parseInt(request.getParameter("delp"));
                Producto productoEliminar = pDao.obtener(codigoEliminar);
                pDao.eliminar(productoEliminar);
                int R_OK = 1;
                response.setContentType("text/plain");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().print(R_OK);
                System.out.println(codigoEliminar);
                break;
            default:
                break;

        }
    }

    //metodo para validar si formulario producto esta vacio
    protected String validacionSave(Producto p, HttpServletRequest request) {
        String val;

        String nom = request.getParameter("nombre");
        String cod = request.getParameter("codigo");
        String pre = request.getParameter("precio");
        String cant = request.getParameter("cantidad");
        String est = request.getParameter("estado");

        val = ((cod == null) || (cod.trim().isEmpty()))
                ? "Ingrese un codigo." : null;

        if (val == null) {
            val = ((nom == null) || (nom.trim().isEmpty()))
                    ? "Ingrese nombre." : null;
        }

        if (val == null) {
            val = ((pre == null) || (pre.trim().isEmpty()))
                    ? "Ingrese precio." : null;
        }
        if (val == null) {
            val = ((cant == null) || (cant.trim().isEmpty()))
                    ? "Ingrese cantidad." : null;
        }
        if (val == null) {
            val = ((est == null) || (est.trim().isEmpty()))
                    ? "Ingrese estado del producto." : null;
        }

        if (val == null) {
            p.setCodigo(cod);
            p.setNombre(nom);
            p.setPrecio(Double.parseDouble(pre));
            p.setStock(Integer.parseInt(cant));
            p.setEstado(est);
            val = "insertpro";
        }
        return val;
    }

    //metodo que me permite validar campos para editar un producto
    protected String validacionUpdate(Producto p, HttpServletRequest request) {

        String val;
        String idp = request.getParameter("idproductom");
        String nom = request.getParameter("nombrem");
        String cod = request.getParameter("codigom");
        String pre = request.getParameter("preciom");
        String cant = request.getParameter("cantidadm");
        String est = request.getParameter("estadom");
        System.out.println("-----id " + Integer.parseInt(idp) + "nombre " + nom);

        val = ((cod == null) || (cod.trim().isEmpty()))
                ? "Ingrese un codigo." : null;

        if (val == null) {
            val = ((nom == null) || (nom.trim().isEmpty()))
                    ? "Ingrese nombre." : null;
        }

        if (val == null) {
            val = ((pre == null) || (pre.trim().isEmpty()))
                    ? "Ingrese precio." : null;
        }
        if (val == null) {
            val = ((cant == null) || (cant.trim().isEmpty()))
                    ? "Ingrese cantidad." : null;
        }
        if (val == null) {
            val = ((est == null) || (est.trim().isEmpty()))
                    ? "Ingrese estado del producto." : null;
        }

        if (val == null) {

            p.setId_producto(Integer.parseInt(idp));
            p.setNombre(nom);
            p.setCodigo(cod);
            p.setEstado(est);
            p.setPrecio(Double.parseDouble(pre));
            p.setStock(Integer.parseInt(cant));

            val = "editproduct";
        }
        return val;
    }

    //metodo que me busca un producto para editarlo
    protected void buscarProducto(HttpServletResponse response, int id_produc, DAOProducto pDao) throws IOException {
        if (id_produc >= 1) {
            Producto pEdit = pDao.obtener(id_produc);
            if (pEdit != null) {
                //convierto pEdit en un objeto json
                Gson productoJson = new Gson();
                response.getWriter().write(productoJson.toJson(pEdit));
            } else {
                response.setContentType("text/plain");
                response.getWriter().write("El producto no existe.");
            }
        } else {
            response.setContentType("text/plain");
            response.getWriter().write("Elije un producto.");
        }
    }

    //metodo que me permite guarar un producto
    protected void guardarProducto(Producto p, DAOProducto pDao, String msjValidacion, HttpServletResponse response) throws IOException {

        if (msjValidacion.equals("insertpro")) {
            int R_OK = 1;
            pDao.insertar(p);
            response.setContentType("text/plain");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().print(R_OK);
        } else {
            String r_error = msjValidacion;
            response.setContentType("text/plain");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().print(r_error);
            System.out.println("probando mensaje-->" + msjValidacion);
        }
    }

    //metodo que me permite editar un producto
    protected void actualizarProducto(HttpServletResponse response,
            String msjValiUpdate, Producto p, DAOProducto pdao) throws IOException {

        if (msjValiUpdate.equals("editproduct")) {
            final int OK_OUT = 1;
            pdao.actualizar(p);
            response.setContentType("text/plain");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().print(OK_OUT);
            System.out.println("mensaje edit--> " + OK_OUT);
        } else {
            String ERROR_OUT = msjValiUpdate;
            response.setContentType("text/plain");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().print(ERROR_OUT);
            System.out.println("mensaje edit--> " + ERROR_OUT);
        }

    }

    //metodo que devuelve todos los  productos.
    protected List<Producto> allProduct() {
        DAOProducto daoProducto = new DAOProductoImpl();
        List<Producto> lista = daoProducto.obtenerTodo();
        return lista;
    }

    protected void listarProductos(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<Producto> list = allProduct();
            String json = new Gson().toJson(list);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(json);
            //request.getRequestDispatcher("/WEB-INF/sis/Home.jsp").forward(request, response);

        } catch (Exception e) {
            System.out.println("mensaje " + e.getMessage());
        }
    }

    @Override
    public String getServletInfo() {
        return "Controlador Producto";
    }// </editor-fold>

}
