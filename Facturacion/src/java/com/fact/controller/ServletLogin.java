/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fact.controller;

import com.fact.dao.DAOEmpleado;
import com.fact.dao.DAOEmpleadoImpl;
import com.fact.model.Empleado;
import java.io.IOException;
import java.util.Enumeration;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ya
 */
@WebServlet(name = "ServletLogin", urlPatterns = {"/Login"})
public class ServletLogin extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        // por defecto este servlet redirecciona a index

        //request.getRequestDispatcher("/WEB-INF/sis/index.jsp").forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        String backAction = request.getParameter("back");
        try {
            if (backAction != null) {
                if (backAction.equals("on")) {
                    request.getRequestDispatcher("/WEB-INF/sis/index.jsp").forward(request, response);
                } else {
                    request.getRequestDispatcher("/WEB-INF/sis/index.jsp").forward(request, response);
                }
            } else {
                request.getRequestDispatcher("/WEB-INF/sis/index.jsp").forward(request, response);
            }
        } catch (Exception e) {
            request.getRequestDispatcher("/WEB-INF/sis/index.jsp").forward(request, response);
            System.out.println("LOg--> " + e.getMessage());
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        String msBd = null;
        String acc = request.getParameter("accion");
        try {
            if (acc.equals("vbs")) {

                String user = request.getParameter("usuario");
                String pass = request.getParameter("password");
                String msg, msu, msp = null;

                DAOEmpleado daoEmp = new DAOEmpleadoImpl();

                if (user == null || user.trim().isEmpty()) {
                    // msu = "Debe ingresar su usuario.";
                    // request.getSession().setAttribute("msu", msu);
                    request.getRequestDispatcher("/WEB-INF/sis/index.jsp").forward(request, response);

                } else if (pass == null || pass.trim().isEmpty()) {
                    // msp = "Debe ingresar una contraseña";
                    //request.getSession().setAttribute("msp", msp);
                    request.getRequestDispatcher("/WEB-INF/sis/index.jsp").forward(request, response);
                } else {
                    Empleado empleado = daoEmp.login(user, pass);
                    if (empleado != null) {
                        if (empleado.getUsuario().equals(user) && empleado.getPassword().equals(pass)) {
                            HttpSession sessionLogin = request.getSession();
                            sessionLogin.setAttribute("empleado", empleado);
                            sessionLogin.setMaxInactiveInterval(30 * 60);//treinta min de sesion
                            request.getRequestDispatcher("/WEB-INF/sis/Home.jsp").forward(request, response);
                            // response.sendRedirect("sis/Home.jsp");
                        }
                    } else {

                        msg = "Usuario o contraseña incorrectos.";
                        request.getSession().setAttribute("msj", msg);
                        request.getRequestDispatcher("/WEB-INF/sis/index.jsp").forward(request, response);
                    }
                }
            } else {
                request.getRequestDispatcher("/WEB-INF/sis/index.jsp").forward(request, response);
            }
        } catch (Exception e) {
            msBd = "¡Atención!,revise su fuente de datos.";
            System.out.println("BASE DE DATOS INACTIVA");
           
                request.getSession().setAttribute("bdOut", msBd);
        
            request.getRequestDispatcher("/WEB-INF/sis/index.jsp").forward(request, response);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
