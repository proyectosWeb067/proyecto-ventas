<%-- 
    Document   : Home
    Created on : 22/09/2020, 02:58:52 PM
    Author     : ya
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!--Icon-->
        <link rel="icon">
        <!-- Bootstrap -->
        <!--link rel="canonical" href="https://getbootstrap.com/docs/3.3/examples/carousel/"-->
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <style>
            .carousel-inner > .item > img,
            .carousel-inner > .item > a > img {

                width: 40%;
                margin: auto;
            }
        </style>
        <title>Inicio</title>
    </head>
    <body>
        <%@ include file="/WEB-INF/jspf/navegador.jspf" %><!--fracment navegador-->
        <div class="container">

            <h1 style="text-align:center;">BIENVENIDO</h1> 

            <!-- Carousel
    ================================================== -->
            <h2>Articulos con stock muy bajo.</h2>
            <div id="myCarousel" class="carousel slide">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li class="item1 active"></li>
                    <li class="item2"></li>
                    <li class="item3"></li>
                    <li class="item4"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <!--estructura dinamica-->
                    <div class="item active">
                        <img src="img/teclado.jpg" alt="Chania" width="260" height="145">
                        <div class="carousel-caption">
                            <h3>Chania</h3>
                            <p>The atmosphere in Chania has a touch of Florence and Venice.</p>
                        </div>
                    </div>

                    <div class="item">
                        <img src="img/pc.jpg" alt="Chania" width="260" height="145">
                        <div class="carousel-caption">
                            <h3>Chania</h3>
                            <p>The atmosphere in Chania has a touch of Florence and Venice.</p>
                        </div>
                    </div>

                    <div class="item">
                        <img src="img/teclado.jpg" alt="Flower" width="260" height="145">
                        <div class="carousel-caption">
                            <h3>Flowers</h3>
                            <p>Beautiful flowers in Kolymbari, Crete.</p>
                        </div>
                    </div>

                    <div class="item">
                        <img src="img/pc.jpg" alt="Flower" width="260" height="145">
                        <div class="carousel-caption">
                            <h3>Flowers</h3>
                            <p>Beautiful flowers in Kolymbari, Crete.</p>
                        </div>
                    </div>

                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <!-- /.carousel -->

            <script src="js/npm.js"></script>
            <!-- Include all compiled plugins (below), or include individual files as needed -->

            <script src="js/bootstrap.js"></script>

            <script src="js/jquery.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script>
                $(document).ready(function () {
                    // Activate Carousel
                    $("#myCarousel").carousel();

                    // Enable Carousel Indicators
                    $(".item1").click(function () {
                        $("#myCarousel").carousel(0);
                    });
                    $(".item2").click(function () {
                        $("#myCarousel").carousel(1);
                    });
                    $(".item3").click(function () {
                        $("#myCarousel").carousel(2);
                    });
                    $(".item4").click(function () {
                        $("#myCarousel").carousel(3);
                    });

                    // Enable Carousel Controls
                    $(".left").click(function () {
                        $("#myCarousel").carousel("prev");
                    });
                    $(".right").click(function () {
                        $("#myCarousel").carousel("next");
                    });
                });
            </script>
    </body>
</html>
