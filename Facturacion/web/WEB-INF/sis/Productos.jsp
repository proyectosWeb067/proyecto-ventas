<%-- 
    Document   : Producto
    Created on : 22/09/2020, 03:21:43 PM
    Author     : ya
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" type="text/css"  rel="stylesheet">
        <link href="css/bootstrap-theme.min.css" type="text/css"rel="stylesheet">
        <link href="css/estilos.css" type="text/css" rel="stylesheet">
        <title>Productos</title>
        <link rel="stylesheet" type="text/css" href="librerias/datatable/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="librerias/datatable/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" type="text/css" href="librerias/alertify/css/alertify.css">
        <link rel="stylesheet" type="text/css" href="librerias/alertify/css/themes/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="librerias/fontawesome/css/font-awesome.css">



    </head>
    <body>
        <%@ include file="/WEB-INF/jspf/navegador.jspf" %> <!--navigator-->

        <div class="container">
            <div id="divPanel" class="panel panel-info">
                <p id="validaCampos"></p> 
                <div class="row bordeFrmP">

                    <form id="frmAdd">
                        <div class=" form-group col-xs-2">
                            <label >CODIGO</label>
                            <input id="codigo" type="text" class="form-control" name="codigo" required="true" />
                        </div>
                        <div class=" form-group col-xs-3">
                            <label >NOMBRE</label>
                            <input id="nombre"type="text" class="form-control" name="nombre" required="true" />
                        </div>
                        <div class="form-group col-xs-2">
                            <label >PRECIO</label>
                            <input id="precio" type="text" name="precio" class="form-control" required="true"/>
                        </div>
                        <div class="form-group col-xs-1">
                            <label>CANT</label>
                            <input id="cantidad"type="text" name="cantidad" class="form-control" required="true"/>
                        </div>
                        <div class="form-group col-xs-2">
                            <label class="text-center">ESTADO</label>
                            <select id="estado" class="form-control" name="estado" >
                                <option value="A">A</option>
                                <option value="I">I</option>
                            </select>
                        </div>
                        <div class="form-actions col-xs-1">
                            <label class="text-center">ACCION</label>
                            <button id="add" type="button" class="btn btn-success form-control">
                                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                            </button> 
                        </div>  
                    </form>
                </div>
            </div>
            <!-- Modal -->
            <div class="modal fade bs-example-modal-sm" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                <div class="modal-dialog modal-sm" role="document" >
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Actualizar Producto</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form id="frmEdit">
                                <input type="hidden" class="form-control input-sm"  hidden="" id="idproductom" name="idproductom">

                                <label>Codigo</label>
                                <input type="text" class="form-control input-sm" id="codigom" name="codigom">

                                <label>Nombre</label>
                                <input type="text" class="form-control input-sm" id="nombrem" name="nombrem">


                                <label>Precio</label>
                                <input type="text" class="form-control input-sm" id="preciom" name="preciom">
                                <label>Cantidad</label>
                                <input type="text" class="form-control input-sm" id="cantidadm" name="cantidadm">
                                <label>Estado</label><br />
                                <select id="estadom" name="estadom" class="" required="true">
                                    <option value="A">A</option>
                                    <option value="I">I</option>
                                </select>
                                <div class="modal-footer">
                                    <button id="cerrarModal" type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                    <button type="button" class="btn btn-warning glyphicon glyphicon-check" id="update">Actualizar</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>


            <div class="panel panel-info">
                <div class="panel-heading"> Tus Productos </div>
                <div class="panel-body"></div>
                <!-- Table -->
                <table id="Datatable" class="table table-striped table-bordered">
                    <thead>
                    <th>CODIGO</th>
                    <th>PRODUCTO</th>
                    <th>PRECIO</th>
                    <th>CANTIDAD</th>
                    <th>ESTADO</th>
                    <th style="text-align: center;">-*-</th>
                    </thead>
                    <tbody>
                        <c:forEach var="lip" items="${lstp}">
                            <tr>
                                <td>${lip.codigo}</td>
                                <td>${lip.nombre}</td>
                                <td>${lip.precio}</td>
                                <td>${lip.stock}</td>
                                <td>${lip.estado}</td>
                                <td>
                                    <div class="form-group text-center">
                                        <button onclick="edit(${lip.id_producto})" type="button" class="btn btn-warning" data-toggle="modal" data-target="#modalEdit">
                                            <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                        </button> 

                                        <button onclick="del(${lip.id_producto})" type="button" class="btn btn-danger" id="delete" >
                                            <span class="glyphicon glyphicon-trash " aria-hidden="true"></span>
                                        </button> 
                                    </div>

                                </td> 
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!--librerias externas-->
    <script src="librerias/jquery.min.js"></script>
    <script src="librerias/bootstrap/popper.min.js"></script>
    <script src="librerias/bootstrap/bootstrap.min.js"></script>
    <script src="librerias/datatable/jquery.dataTables.min.js"></script>
    <script src="librerias/datatable/dataTables.bootstrap4.min.js"></script>
    <script src="librerias/alertify/alertify.js"></script>

    <script type="text/javascript">
                                            $(document).ready(function () {
                                                $('#Datatable').DataTable();
                                            });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#add').click(function () {
                let datos = $('#frmAdd').serialize();
                let ss = "save";
                $.ajax({
                    type: 'POST',
                    data: datos + '&actionp=' + ss,
                    url: 'Producto',
                    success: function (response) {
                        if (response == 1) {
                            $('#frmAdd')[0].reset();
                            var divP = document.getElementById("divPanel");
                            var val = document.getElementById("validaCampos");
                            val.textContent = "¡Correcto!,se guardo tu producto.";
                            val.className = "label label-success";
                            divP.append(val);
                            alertify.success("agregado con exito :D");
                            console.log("respusta " + response);
                        } else {
                            var divP = document.getElementById("divPanel");
                            var val = document.getElementById("validaCampos");
                            val.textContent = response;
                            val.className = "label label-danger";
                            divP.append(val);
                            alertify.error("¡Rellena todos los campos para guardar! :(");
                        }
                    },
                    error: function () {
                        console.log("No se ha podido obtener la información");
                    }
                });
            });
            $('#update').click(function () {
                let edit = $('#frmEdit').serialize();
                let su = 'update';
                $.ajax({
                    url: 'Producto',
                    type: 'POST',
                    data: edit + '&actionp=' + su,
                    success: function (responseEdit) {
                        if (responseEdit == 1) {
                            $('#frmEdit')[0].reset();
                            alertify.success("¡Perfecto!,has actualizado tu producto.");
                        } else {
                            alertify.error("¡Atencion!,sucedio algo inesperado.");
                        }
                    },
                    error: function () {

                    }
                });
                $('#update').attr("data-dismiss", "modal");
                //actualizando página
                window.setInterval(function () {
                    location.reload();
                }, 3000);
            });
        });
    </script>

    <script type="text/javascript">
        function edit(cod) {
            let se = "edit";
            $.ajax({
                type: 'POST',
                url: 'Producto',
                data: 'inep=' + cod + '&actionp=' + se,
                success: function (response) {
                    var obj = JSON.parse(response);
                    console.log(obj.id_producto);
                    $('#idproductom').val(obj.id_producto);
                    $('#codigom').val(obj.codigo);
                    $('#nombrem').val(obj.nombre);
                    $('#preciom').val(obj.precio);
                    $('#cantidadm').val(obj.stock);
                    $('#estadom').val(obj.estado);
                },
                error: function () {

                }
            });
        }

        function del(codigo) {
            let setDel = "del";
            $.ajax({
                type: 'POST',
                url: 'Producto',
                data: 'delp=' + codigo + '&actionp=' + setDel,
                success: function (response) {
                    if (response == 1) {
                        alertify.success("¡Perfecto!,producto eliminado.");
                        //actualizando página
                        window.setInterval(function () {
                            location.reload();
                        }, 3000);
                    }

                },
                error: function () {
                     alertify.error("¡Error!,consulte al administrador.");
                }
            });
        }
    </script> 
</body>
</html>