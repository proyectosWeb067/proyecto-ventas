<%-- 
    Document   : rp_out
    Created on : 2/10/2020, 09:39:58 PM
    Author     : ya
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap -->
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <title>Error</title>
    </head>
    <body>
        <div class="container">
            <div class="alert alert-danger" role="alert">
                <h3>${err}</h3>
            </div>
            <!--redreccionar a un servlet-->
            <a class="btn btn-danger" href="Login?back=on">Inicia Sesión.</a>
        </div>
            
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.js"></script>
    </body>
</html>
