<%-- 
    Document   : msj
    Created on : 24/09/2020, 11:59:22 PM
    Author     : ya
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <!-- Bootstrap -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <title>Error</title>
    </head>
    <body>
        <div class="container">
            <h3>ERROR,VERIFIQUE POR FAVOR!</h3>
            <p>${msg}</p><br /><br />

            <a class="btn" href="index.jsp">Regresar</a>
        </div>

    </body>
</html>
